class CashflowIssue < Issue
  def custom_field_values
    values = super

    # If we have the transaction_type Issue Custom Field with the value of "Income"
    transaction_type = values.find { |field| field.custom_field.label == "transaction_type"}

    # We should remove the budget_item
    if !transaction_type.nil? && ['Income', 'Receita'].include?(transaction_type.value())
      values.reject { |field| field.custom_field.label == "budget_item" }
    else
      values
    end
  end

end
