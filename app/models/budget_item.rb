class BudgetItem < ActiveRecord::Base
  unloadable
  belongs_to :category, class_name: 'BudgetCategory', foreign_key: :budget_category_id
  validates_presence_of :description

  has_many :journals, :as => :journalized, :dependent => :destroy
  def notified_users
    []
  end
  alias :notified_watchers :notified_users
  after_create do |item|
    item.journals.create!(user: User.current, notes: I18n.t('budget_items.created'))
  end
  after_update do |item|
    journal = item.journals.new(user: User.current, notes: I18n.t('budget_items.updated'))
    if item.changed_attributes.has_key?('removed')
      journal.notes = I18n.t('budget_items.removed')
    else
      item.changed_attributes.each do |k,v|
        if k == 'tax_table_id'
          prop_name = I18n.t("budget_items.tax_table")
          old_value =
            begin
              TaxTable.find(item.tax_table_id_was).name
            rescue ActiveRecord::RecordNotFound
              '-'
            end
          new_value = item.tax_table.name
        else
          prop_name = I18n.t("budget_items.#{k}")
          old_value = v
          new_value = item.send(k)
        end
        journal.details << JournalDetail.new(
          property: prop_name,
          prop_key: k,
          old_value: old_value,
          value: new_value,
        )
      end
    end
    journal.save!
  end

  belongs_to :tax_table

  validates_each :quantity do |item,attr,value|
    if !item.months.empty? && value && value != item.months.size
      item.errors.add(I18n.t('budget_items.quantity'), I18n.t('budget_items.errors.quantity_does_not_match_months') % item.months.size)
    end
  end

  def total
    total_before_taxes && (total_before_taxes + total_taxes)
  end

  def total_before_taxes
    quantity && value && quantity * value
  end

  def total_taxes
    quantity * tax
  end

  def taxes
    if tax_table
      tax_table.taxes_for(value)
    else
      []
    end
  end

  def tax
    taxes.inject(0) { |acc, tax| acc += tax.result; acc }
  end

  def months
    String(month_spec).split(/[^0-9-]/).map do |item|
      parts = item.split('-')
      parts.reject!(&:empty?)
      if parts.size < 2
        parts << parts.first
      end
      parts.map!(&:to_i)
      (parts[0]..parts[1]).to_a
    end.flatten
  end

  def to_s
    description
  end

  class BatchInvalid < Exception
    def initialize(records)
      @records = records
    end
    attr_reader :records
  end

  class << self

    def batch(category, items, original_items=nil)
      original_items ||= category.items
      output_items = []
      invalid = false
      transaction do
        filter(items).each do |attrs|
          id = attrs[:id] || attrs['id']
          if id.blank?
            id = nil
          end
          delete = attrs[:_delete] || attrs['_delete']
          if id
            item = original_items.find(id)
            if delete
              item.removed = true
              item.save!
            else
              attrs.each do |k,v|
                item.send("#{k}=", v)
              end
              if item.valid?
                item.save!
              else
                invalid = true
              end
            end
            output_items << item
          else
            item = original_items.build(attrs)
            if item.valid?
              item.save!
            else
              invalid = true
            end
            output_items << item
          end
        end
        if invalid
          raise BatchInvalid.new(output_items)
        end
      end
    end

    private

    def filter(items)
      items.reject do |item|
        item.empty? || item.values.all?(&:blank?)
      end
    end
  end
end
