# encoding: UTF-8

require 'forwardable'

# This class represents the budget schedule, or expense schedule. In Brazil
# this is called "Physical-Financial schedule" ("Cronograma físico-financeiro")
#
# The budget schedule is basically a matrix with categories in the rows and
# months in the columns. Each cell represents how much money we expect to spend
# for that budget category on that month of the project.
class BudgetSchedule

  attr_reader :budget
  attr_reader :rows
  attr_reader :number_of_months

  extend Forwardable
  delegate :each => :rows

  def initialize(budget)
    @budget = budget
    calculate_number_of_months
    fill_in_cells
  end

  class Row
    extend Forwardable
    attr_accessor :name
    def initialize(name)
      @name = name
    end
    delegate [:each, :<<, :[] ] => :cells
    def cells
      @cells ||= []
    end
    def total
      cells.map(&:amount).sum
    end
  end

  class NumberCell
    attr_reader :amount
    def initialize(label = nil)
      @amount = 0
      @label = label
    end
    def label
      empty? && 'empty' || (@label || 'filled')
    end
    def add(amount)
      @amount += amount
    end
    def empty?
      @amount == 0
    end
  end

  private

  def calculate_number_of_months
    @number_of_months = 0
    process_budget do |category|
      category.items.each do |item|
        m = item.months.max
        if m && m > @number_of_months
          @number_of_months = m
        end
      end
    end
  end

  def fill_in_cells
    @rows = []
    process_budget do |category|
      row = Row.new(category.name)

      unscheduled = NumberCell.new(:unscheduled)
      row << unscheduled
      (1..number_of_months).each { row << NumberCell.new }

      category.items.each do |item|
        if item.months.empty?
          unscheduled.add(item.total)
        else
          unscheduled_value = item.total
          (1..number_of_months).each do |month|
            if item.months.include?(month)
              row[month].add(item.value)
              unscheduled_value -= item.value
            end
          end
          if unscheduled_value > 0
            unscheduled.add(unscheduled_value)
          end
        end
      end

      @rows << row
    end
  end

  private

  def process_budget(&block)
    budget.categories.each do |category|
      process_category(category, &block)
    end
  end

  def process_category(category, &block)
    if category.items.empty?
      category.children.each do |subcategory|
        process_category(subcategory, &block)
      end
    else
      block.call(category)
    end
  end

end
