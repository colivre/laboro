
require 'bigdecimal'

class Irpf

  INF = BigDecimal::INFINITY

  INSS = {
    2015 => [
      { lim: 1399.12, aliq: 0.08, sum: 0.0 },
      { lim: 2331.88, aliq: 0.09, sum: 0.0 },
      { lim: 4663.75, aliq: 0.11, sum: 0.0 },
      { lim: INF,     aliq: 0.00, sum: 513.01 },
    ]
  }

  IRPF = {
    2015 => [
      { lim: 1903.98, aliq: 0,     disc: 0 },
      { lim: 2826.65, aliq: 0.075, disc: 142.80 },
      { lim: 3751.05, aliq: 0.15,  disc: 354.80 },
      { lim: 4664.68, aliq: 0.225, disc: 636.13 },
      { lim: INF,     aliq: 0.275, disc: 869.36 },
    ]
  }

  def initialize(year)
    @inss = INSS.fetch(year.to_i)
    @irpf = IRPF.fetch(year.to_i)
  end

  def inss(br)
    entry = @inss.find { |e| br < e[:lim] }
    __inss(br, entry)
  end

  def irpf(bc)
    entry = @irpf.find { |e| bc <= e[:lim] }
    __irpf(bc, entry)
  end

  def irpf_inss_br(br)
    { inss: inss(br), irpf: irpf(br - inss(br))  }
  end

  def liq(br, al_iss=0)
    br - inss(br) - irpf(br-inss(br)) - al_iss*br
  end

  def find_br(liq, al_iss=0)
    liq = BigDecimal.new(liq.to_s)

    lower = liq.to_f
    upper = liq.to_f
    while (liq(upper, al_iss) <= liq)
      upper *= 2
    end

    # binary search
    while (upper - lower) > 0.0001
      diff0 = (liq(lower, al_iss) - liq).abs
      diff1 = (liq(upper, al_iss) - liq).abs
      middle = lower + (upper-lower)/2
      if (diff0 < diff1)
        upper = middle
      else
        lower = middle
      end
    end
    lower
  end

  private

  def __inss(br, entry)
    br * entry[:aliq] + entry[:sum]
  end

  def __irpf(bc, entry)
    bc * entry[:aliq] - entry[:disc]
  end

end
