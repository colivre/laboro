class BudgetDiff

  attr_reader :removed, :added, :changed
  attr_reader :old_budget, :new_budget

  Change = Struct.new(:old, :new)

  def initialize(old_budget, new_budget)
    @removed = []
    @added = []
    @changed = []
    @old_budget = old_budget
    @new_budget = new_budget

    calculate!
  end

  def empty?
    [removed, added, changed].all?(&:empty?)
  end

  private # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  def calculate!
    old_items = dump_categories(old_budget.categories)
    new_items = dump_categories(new_budget.categories)

    old_items.each do |item|
      if !new_items.include?(item)
        removed << item
      end
    end

    new_items.each do |item|
      if old_items.include?(item)
        old_item = old_items.find { |i| i == item }
        if old_item.attributes != item.attributes
          changed << Change.new(old_item, item)
        end
      else
        added << item
      end
    end
  end

  def dump_categories(categories)
    list = []
    categories.each do |c|
      dump_items(c, list)
    end
    list
  end

  def dump_items(category, stream)
    category.items.each do |item|
      stream << item
    end
    category.children.each do |c|
      dump_items(c, stream)
    end
  end

end
