class BudgetSnapshot < ActiveRecord::Base
  unloadable

  serialize :data, JSON
  alias :current? :new_record?

  belongs_to :project

  ProjectWrapper = Struct.new(:id, :categories) do
    class << self
      def wrap(project)
        if project.is_a?(self)
          project
        else
          self.new(project.id, BudgetCategory.by_project(project).roots)
        end
      end
    end
  end

  def description
    new_record? && I18n.t('budget.current') || I18n.l(timestamp)
  end

  def timestamp
    (created_at || Time.now).in_time_zone(User.current.time_zone || ::Time.zone)
  end

  def previous
    previous = self.class.where(project_id: project_id).order('created_at DESC')
    if self.id
      previous = previous.where(["id < ?", self.id])
    end
    previous.first
  end

  def total
    categories.map(&:total).sum
  end

  def categories
    @categories ||= Array(data).map { |c| load_category(c) }
  end

  def categories=(categories)
    @categories = categories
    self.data = categories.map { |c| dump_category(c) }
  end

  class << self

    def build(project)
      project = BudgetSnapshot::ProjectWrapper.wrap(project)
      self.new(project_id: project.id, categories: project.categories)
    end
  end

  private

  def load_category(data)
    c = BudgetCategory.new
    c.items = load_items(data.delete('items'))
    c.children = Array(data.delete('children')).map { |c| load_category(c) }
    c.id = data['id']
    c.attributes = sanitize(data)
    c
  end

  def load_items(data)
    Array(data).map do |item_attrs|
      BudgetItem.new(sanitize(item_attrs)).tap do |item|
        item.id = item_attrs['id']
      end
    end
  end

  def dump_category(category)
    data = category.attributes.dup
    data['children'] = Array(category.children).map { |c| dump_category(c) }
    data['items'] = Array(category.items).map { |i| i.attributes }
    data
  end

  def sanitize(data)
    data.reject { |k,v| k == 'id' }
  end

end
