class CreateIssuesFromBudget

  Issue = Struct.new(:subject, :due_date, :value, :budget_item_id)

  def initialize(budget, start_date)
    @budget = budget
    @start_date = start_date
    process_budget
  end

  attr_reader :budget, :start_date

  def issues
    @issues ||= []
  end

  def create_issues!
    project = budget.project
    tracker = project.trackers.where(label: 'cashflow').first

    unless tracker
      # Force-enable "Cashflow" tracker in the project
      tracker = Tracker.where(label: 'cashflow').first
      project.trackers << tracker
    end

    issues.map do |issue_data|
      issue = ::Issue.new
      issue.project = project
      issue.tracker = tracker

      issue.subject = issue_data.subject
      issue.due_date = issue_data.due_date

      issue.author = User.current

      value = CustomValue.new
      value.custom_field = IssueCustomField.where(label: "transaction_value").first
      value.value = issue_data.value.to_s
      issue.custom_values << value

      budget_item = CustomValue.new
      budget_item.custom_field = IssueCustomField.where(label: "budget_item").first
      budget_item.value = issue_data.budget_item_id.to_s
      issue.custom_values << budget_item

      issue.save!
      issue
    end
  end

  private

  def process_budget
    budget.categories.each do |category|
      process_category(category)
    end
  end


  def process_category(category)
    category.children.each do |subcategory|
      process_category(subcategory)
    end
    category.items.each do |item|
      process_item(item)
    end
  end

  def process_item(item)
    if item.months.empty?
      issues << Issue.new(item.description, nil, item.total, item.id)
      process_taxes(item)
    else
      item.months.each_with_index do |n,i|
        issue = Issue.new
        suffix = "#{i+1}/#{item.months.size}"
        issue.subject = "#{item.description} #{suffix}"
        issue.value = item.value
        issue.budget_item_id = item.id
        issue.due_date = start_date + n.months
        issues << issue
        process_taxes(item, suffix, issue.due_date)
      end
    end
    issues.sort_by! { |item| item.due_date || Date.new(0) }
  end

  def process_taxes(item, suffix=nil, due_date=nil)
    item.taxes.each do |tax|
      issue = Issue.new
      issue.subject = [
        item.description, "(#{tax.name})", suffix
      ].compact.join(' ')
      issue.value = tax.result
      issue.budget_item_id = item.id
      issue.due_date = due_date
      issues << issue
    end
  end

end
