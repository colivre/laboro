require 'ruby_parser'

class TaxTable < ActiveRecord::Base
  unloadable

  belongs_to :project
  has_many :budget_items, :dependent => :restrict

  validates_presence_of :name

  scope :available_for, lambda { |project| where(['project_id is NULL or project_id = ?', project.id]) }

  scope :belonging_to, lambda { |project| where(project_id: project.id) }

  scope :global, where('project_id is NULL')

  def global?
    project_id.nil?
  end

  class Entry
    attr_reader :name
    attr_reader :variable
    attr_reader :expression
    attr_accessor :result

    def initialize(data)
      data.each do |k,v|
        self.instance_variable_set("@#{k}", v)
      end
    end

    def evaluate!(binding)
      self.result = eval(expression, binding)
      if numeric?
        self.result = self.result.round(2)
      end
      self.result
    end

    def numeric?
      result.is_a?(Numeric)
    end

    def resolvable_and_safe?(variables)
      parser = RubyParser.new
      code = parser.parse(expression)
      __check(code, variables)#.tap { puts '-' * 20}
    end

    def __check(sexp, variables)
      if sexp.nil?
        return true
      end

      case sexp.first
      when :call
        if sexp[1] == nil
          variables.include?(sexp[2].to_s) || sexp[2] == :lambda
        else
          __check(sexp[1], variables) && __check(sexp[3], variables)
        end
      when :arglist, :iter, :or, :and, :if
        sexp[1..-1].all? { |s| __check(s, variables) }
      when :lasgn, :lvar
        true
      when :lit
        sexp[1].is_a?(Numeric)
      else
        false
      end#.tap { |v| puts [sexp, v].inspect }
    end
  end

  class Context
    attr_reader :input
    def initialize(input)
      @input = input
    end
    def get_binding
      binding
    end
    def set_irpf(year)
      @irpf ||= Irpf.new(year)
    end
    # FIXME portuguese
    def bruto_com_irpf_inss(v, al_iss=0)
      @irpf.find_br(v, al_iss)
    end
    def inss(v)
      @irpf.irpf_inss_br(v)[:inss]
    end
    def irpf(v)
      @irpf.irpf_inss_br(v)[:irpf]
    end
  end

  serialize :entries, Array

  def taxes_for(input)
    context = TaxTable::Context.new(input)

    entries.map do |data|
      entry = TaxTable::Entry.new(data)

      value = entry.evaluate!(context.get_binding)

      # inject value in the context so it can be used when calculating the next
      # entry
      case value
      when Proc
        context.define_singleton_method(entry.variable) do |*args|
          value.call(*args)
        end
      else
        context.define_singleton_method(entry.variable) { value }
      end

      entry
    end.select(&:numeric?)
  end

  validate :validate_expressions

  def validate_expressions
    variables = ['input', 'set_irpf', 'bruto_com_irpf_inss', 'inss', 'irpf']
    entries.each do |e|
      entry = TaxTable::Entry.new(e)

      unless entry.resolvable_and_safe?(variables)
        errors.add(:entries, '[%s] cannot be resolved' % entry.expression)
      end

      variables << entry.variable
    end
  end

end
