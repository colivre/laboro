class BudgetCategory < ActiveRecord::Base
  unloadable
  acts_as_tree dependent: :destroy
  belongs_to :project
  validates_presence_of :project_id, :name
  scope :by_project, lambda { |project| where(project_id: project.id) }
  has_many :items, class_name: 'BudgetItem', conditions: { removed: false }

  def total
    if items.empty?
      children.map(&:total).sum
    else
      items.map(&:total).compact.sum
    end
  end
end
