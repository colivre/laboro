class Cashflow < ActiveRecord::Base
  unloadable

  def self.transaction_custom_field
    IssueCustomField.where(:label => "transaction_value").first
  end

  def self.budget_item_custom_field
    IssueCustomField.where(:label => "budget_item").first
  end

  def self.transaction_type_custom_field
    IssueCustomField.where(:label => "transaction_type").first
  end

  def self.custom_field_description
    "cf_#{transaction_custom_field().id}"
  end

  def self.budget_item_field_description
    "cf_#{budget_item_custom_field().id}"
  end

  def self.transaction_type_field_description
    "cf_#{transaction_type_custom_field().id}"
  end

  def self.tracker
    Tracker.where(:label => "cashflow").first
  end

  def self.get_query(project)
    IssueQuery.new(:name => "_", :project => project)
  end

end
