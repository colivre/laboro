class BudgetItemsController < ApplicationController
  unloadable

  menu_item :budget

  def edit
    @category = BudgetCategory.find(params[:category_id])

    @items = @category.items
    @tax_tables = tax_tables
    if request.post?
      begin
        BudgetItem.batch(@category, params[:budget_items], @items)
        redirect_to controller: 'budget'
      rescue BudgetItem::BatchInvalid => e
        @items = e.records
        flash[:error] = format_error_messages(e.records)
      end
    end
  end

  def show
    @item = BudgetItem.find(params[:id])
    @project = Project.find(params[:project_id])
    if @item.category.project != @project
      raise ArgumentError.new('Item not in this project')
    end
  end

  private

  def tax_tables
    TaxTable.available_for(@category.project).all.map do |t|
      [t.name, t.id]
    end
  end

  def format_error_messages(records)
    msgs = []
    records.each_with_index do |item, i|
      item.errors.full_messages.each do |m|
        description = item.description
        if description.blank?
          description = I18n.t('budget_items.without_description') % (i+1)
        end
        msgs << "#{description}: #{m}"
      end
    end
    "<ul>#{msgs.map { |m| "<li>#{m}</li>" }.join}</ul>"
  end

end
