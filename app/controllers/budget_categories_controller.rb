class BudgetCategoriesController < ApplicationController
  unloadable
  menu_item :budget

  before_filter :find_project_by_project_id
  before_filter :get_categories

  def bulk_add
    if request.post?
      categories = parse(params[:data])
      categories.each do |c|
        c.project = @project
      end
      categories.each do |c|
        c.save!
      end
      redirect_to controller: 'budget', action: :index
    end
  end

  def destroy
    BudgetCategory.by_project(@project).find(params[:id]).destroy
    redirect_to action: :index
  end

  def edit
    @budget_category = BudgetCategory.by_project(@project).find(params[:id])
    if request.post?
      @budget_category.update_attributes(params[:budget_category])
      redirect_to action: :index
    end
  end

  def new
    @budget_category = BudgetCategory.new(params[:budget_category])
    @budget_category.project = @project
    if params[:parent_id]
      @budget_category.parent = BudgetCategory.by_project(@project).find(params[:parent_id])
    end
    if request.post?
      @budget_category.save!
      redirect_to action: :index
    else
      render action: :edit
    end
  end

  protected

  def get_categories
    @categories = BudgetCategory.by_project(@project).includes(:children)
  end

  def parse(input)
    stack = []
    output = []
    levels = {}
    level = 0
    input.lines.each do |line|
      next if line.strip.empty?
      if line =~ /^(\s+)/
        new_level = $1.size
      else
        new_level = 0
      end
      if new_level <= level
        while !stack.empty? && levels[stack.last] >= new_level
          stack.pop
        end
      end
      level = new_level

      category = BudgetCategory.new(name: line.strip)
      category.parent = stack.last
      stack.push(category)
      output.push(category)
      levels[category] = level
    end
    output
  end

end
