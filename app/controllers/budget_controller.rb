class BudgetController < ApplicationController
  unloadable

  before_filter :find_project_by_project_id

  def index
    @budget = BudgetSnapshot.build(@project)
  end

  def history
    if params[:from_id]
      redirect_to action: 'compare_snapshots', from_id: params[:from_id], to_id: params[:to_id]
      return
    end
    @history = BudgetSnapshot.where(project_id: @project.id).order('created_at')
    @current = BudgetSnapshot.build(@project)
  end

  def snapshot
    budget = BudgetSnapshot.build(@project)
    budget.save!
    redirect_to action: 'index'
  end

  def view_snapshot
    @budget = BudgetSnapshot.where(project_id: @project.id).find(params[:id])
    render 'index'
  end

  def compare_snapshots
    old_budget = BudgetSnapshot.where(project_id: @project.id).find(params[:from_id])
    if params[:to_id]
      new_budget = BudgetSnapshot.where(project_id: @project.id).find(params[:to_id])
    else
      new_budget = BudgetSnapshot.build(@project)
    end
    @diff = BudgetDiff.new(old_budget, new_budget)
  end

  def schedule
    @budget = BudgetSnapshot.build(@project)
    @schedule = BudgetSchedule.new(@budget)
  end

  def create_issues_preview
    unless params[:start_date].blank?
      budget = BudgetSnapshot.build(@project)
      @start_date = Date.parse(params[:start_date])
      @issues = CreateIssuesFromBudget.new(budget, @start_date).issues
    end
  end

  def create_issues
    start_date = Date.parse(params[:start_date])
    budget = BudgetSnapshot.build(@project)
    creator = CreateIssuesFromBudget.new(budget, start_date)
    creator.create_issues!
    redirect_to controller: 'cashflow'
  end

end
