# coding: utf-8
class CashflowIssuesController < CashflowController
  unloadable

  helper :custom_fields
  include CustomFieldsHelper
  before_filter :create_issue, only: [:new, :create, :update_form]
  before_filter :load_issue, only: [:edit, :update]

  def create
    # Redmine hook, let's call it instead of calling the original method
    call_hook(:controller_issues_new_before_save, { :params => params, :issue => @issue })

    if @issue.save
      # Redmine hook, let's call it instead of calling the original method
      call_hook(:controller_issues_new_after_save, { :params => params, :issue => @issue})

      respond_to do |format|
        format.html {
          flash[:notice] = l(:notice_issue_successful_create, :id => view_context.link_to("##{@issue.id}", issue_path(@issue), :title => @issue.subject))
          if params[:continue]
            attrs = {:parent_issue_id => @issue.parent_issue_id}.reject {|k,v| v.nil?}
            redirect_to project_new_cashflow_issue_path(@issue.project, :issue => attrs)
          else
            redirect_to project_cashflow_path
          end
        }
        format.api  { render :action => 'show', :status => :created, :location => issue_url(@issue) }
      end
      return
    else
      respond_to do |format|
        format.html { render :action => 'new' }
        format.api  { render_validation_errors(@issue) }
      end
    end
  end

  def update
    begin
      call_hook(:controller_issues_edit_before_save, { :params => params, :issue => @issue })
      @issue.save!
      call_hook(:controller_issues_edit_after_save, { :params => params, :issue => @issue})
      redirect_to action: 'index'
    rescue ActiveRecord::RecordInvalid
      edit
      render action: 'edit'
    end
  end

  protected

  def create_issue
    @issue = CashflowIssue.new
    fill_issue
  end

  def load_issue
    @issue = CashflowIssue.find(params[:id])
    fill_issue
  end

  def fill_issue
    @issue.project = @project
    @issue.tracker = @tracker

    @issue.author ||= User.current
    @issue.start_date ||= Date.today
    @issue.safe_attributes = params[:issue]

    @allowed_statuses = @issue.new_statuses_allowed_to(User.current)
  end

end
