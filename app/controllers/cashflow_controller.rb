# coding: utf-8
class CashflowController < BudgetController
  unloadable
  menu_item :cashflow

  helper :sort
  include SortHelper

  helper :queries
  include QueriesHelper

  helper :cashflow
  include CashflowHelper

  #PDF imports
  helper :custom_fields
  include CustomFieldsHelper
  include Laboro::PDF

  before_filter :find_custom_fields, :find_tracker, :get_custom_fields_description, :get_transactions
  before_filter :get_sum_of_transaction_values, :get_balance, :only => :index

  def index
    respond_to do |format|
      format.html { render :template => 'cashflow/index', :layout => !request.xhr? }
      format.pdf  {
        @query.save
        @query.name = l(:cashflow_title)
        @query.column_names = [:id, Cashflow.budget_item_field_description, Cashflow.custom_field_description, :due_date, :closed_on ]
        send_data(issues_to_pdf(@transactions, nil, @query), :type => 'application/pdf', :filename => 'cashflow.pdf')
      }
      format.csv  {
        @query.column_names = [:id, Cashflow.budget_item_field_description, Cashflow.custom_field_description, :due_date, :closed_on ]
        send_data(query_to_csv(@transactions, @query, params), :type => 'text/csv; header=present', :filename => 'cashflow.csv')
      }
    end
  end

  def get_custom_fields_description
    @custom_field_desc = Cashflow.custom_field_description
    @transaction_type_field_desc = Cashflow.transaction_type_field_description
    @budget_item_field_desc = Cashflow.budget_item_field_description
  end

  def report_by_month
    @transactions_by_month = @all_transactions.group_by do |issue|
      date = transaction_date(issue)
      [date.year, date.month]
    end

    report_helper(method(:transactions_by_month_helper))
  end

  def report_by_week
    month = params[:month] ? params[:month].to_i : 1
    if (month < 1) or (month > 12)
      flash[:notice] = "Invalid month"
      month = 1
    end

    @transactions_by_week = @all_transactions.select{|issue| transaction_date(issue).month == month }.group_by do |issue|
      date = transaction_date(issue)
      [date.year, date.strftime("%W").to_i]
    end

    report_helper(method(:transactions_by_week_helper))

    # Create a date for the month and @year
    date = Date.new(@year, month)

    @starting_week = date.beginning_of_month.strftime("%W").to_i
    @ending_week = date.end_of_month.strftime("%W").to_i
  end

  def report_helper(transactions_helper)

    @year = params[:year] ? params[:year].to_i : 2015
    @credits = transactions_helper.call lambda {  |transaction| value_for_transaction(transaction, @transaction_custom_field).to_f >= 0 }

    @credits_sum = sum_of_transactions(@credits)

    @credits_by_subject = group_by_category(@credits)

    @debts = transactions_helper.call lambda {  |transaction| value_for_transaction(transaction, @transaction_custom_field).to_f < 0 }
    @debts_sum = sum_of_transactions(@debts)
    @debts_by_subject = group_by_category(@debts)

    @period_balance = [@credits_sum, @debts_sum].transpose.map do |credit, debt|
      credit + debt
    end

    @current_balance = @period_balance.inject([0]) do |sum, value|
      sum << sum[sum.length - 1] + value
    end[1..-1]
  end

  def report
    if params[:month].nil? or params[:month].empty?
      report_by_month
    else
      report_by_week
    end

    respond_to do |format|
      format.html { render :template => 'cashflow/report', :layout => !request.xhr? }
      format.pdf  {

        send_data(report_to_pdf(['credits', @credits_sum, @credits_by_subject], ['debts', @debts_sum, @debts_by_subject], ['month_balance', @period_balance], ['current_balance', @current_balance]), :type => 'application/pdf', :filename => 'cashflow_report.pdf')
      }
    end
  end

  protected

  def transaction_date(issue)
     issue.due_date || issue.closed_on || issue.created_on
  end

  def group_by_category(transactions)
    by_category_or_subject = {}
    transactions.each_with_index do | month_transactions, index |
      month_transactions.each do | transaction |
        type = transaction.custom_value_for(Cashflow.budget_item_custom_field)
        if type.nil?
          description = transaction.subject
        else
          description = BudgetCategory.find(BudgetItem.find(type.value).budget_category_id).name
        end

        by_category_or_subject[description] ||= Array.new(transactions.size, 0)
        by_category_or_subject[description][index] += value_for_transaction(transaction, @transaction_custom_field).to_f
      end
    end
    by_category_or_subject
  end

  def sum_of_transactions(transactions)
    transactions.map do | each_month |
      each_month.map{| transaction | value_for_transaction(transaction, @transaction_custom_field).to_f }.inject(0, &:+)
    end
  end

  def transactions_by_period_helper(selector, number_of_items, group_of_transactions)
    (1..number_of_items).inject([]) do | sums, index |
      transactions = group_of_transactions[[@year, index]]
      value = []
      if !transactions.nil?
        transactions.each do |transaction|
          value << transaction if selector.call transaction
        end
      end
      sums << value
    end

  end

  def transactions_by_month_helper(selector)
    transactions_by_period_helper(selector, 12, @transactions_by_month)
  end

  def transactions_by_week_helper(selector)
    transactions_by_period_helper(selector, 52, @transactions_by_week)
  end

  def find_custom_fields
    @transaction_custom_field = Cashflow.transaction_custom_field
    @transaction_type_custom_field = Cashflow.transaction_type_custom_field
    @budget_item_custom_field = Cashflow.budget_item_custom_field
  end

  def find_tracker
    @tracker = Cashflow.tracker
  end

  def get_transactions
    # This IssueQuery of Redmine help us in getting the right
    # values for custom field values
    @query = Cashflow.get_query(@project)

    @all_transactions = @query.issues

    @query.filters = {}
    @query.add_short_filter("tracker_id", @tracker.id.to_s)

    ["subject", @custom_field_desc, @budget_item_field_desc, @transaction_type_field_desc, "start_date"].each do |field|
      if params[field] and not params[field].empty?
        @query.add_filter(field, "~", [params[field]])
      end
    end

    sort_init('due_date')
    sort_update(@query.sortable_columns)

    @transactions = @query.issues(:order => sort_clause)
  end

  def get_sum_of_transaction_values
    @sum_of_transaction_values = sum_of_transaction_values(@transactions, @transaction_custom_field.id)
  end

  def get_balance
    @balance = sum_of_transaction_values(@all_transactions, @transaction_custom_field.id)
  end
end
