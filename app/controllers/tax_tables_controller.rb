class TaxTablesController < ApplicationController
  unloadable

  menu_item :budget
  before_filter :find_project_by_project_id

  def index
    @tax_tables = readable_tax_tables.order(:id)
  end

  def new
    if request.post?
      @tax_table = TaxTable.new(params[:tax_table])
      save(@tax_table)
    else
      @tax_table = TaxTable.new
      if params[:copy]
        @tax_table.entries = readable_tax_tables.find(params[:copy]).entries
      end
    end
  end

  def edit
    @tax_table = editable_tax_tables.find(params[:id])
    if request.post?
      @tax_table.update_attributes(params[:tax_table])
      save(@tax_table)
    end
  end

  def show
    @tax_table = readable_tax_tables.find(params[:id])
  end

  def destroy
    if request.post?
      begin
        tax_table = editable_tax_tables.find(params[:id])
        tax_table.destroy
      rescue ActiveRecord::DeleteRestrictionError
        flash[:notice] = I18n.t(:'tax_table.cannot_remove')
      end
    end
    redirect_to :action => 'index'
  end

  private

  def readable_tax_tables
    TaxTable.available_for(@project)
  end

  def editable_tax_tables
    TaxTable.belonging_to(@project)
  end

  def save(tax_table)
    tax_table.project = @project
    begin
      tax_table.save!
      redirect_to :action => 'index'
    rescue ActiveRecord::RecordInvalid
      # nothing, just render again
    end
  end

end
