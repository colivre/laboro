# encoding: UTF-8
#
module BudgetHelper

  def display_change(old, new)
    if old == new
      content_tag('span', old, class: 'no-change')
    else
      content_tag('em', old) + ' → ' + content_tag('strong', new)
    end
  end

  def display_journal_users(item, diff)
    journals = item.journals.where(
      [
        'created_on >= ? AND created_on <= ?',
        diff.old_budget.timestamp,
        diff.new_budget.timestamp
      ]
    )
    journals.map { |j| j.user.name }.uniq.join(', ')
  end

end
