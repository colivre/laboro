module CashflowHelper
  def value_for_transaction(transaction, custom_field_id, default_value="")
    cv = transaction.custom_value_for(custom_field_id)
    unless cv.nil?
      cv.value
    else
      default_value
    end
  end

  def column_content_for(query, column_name, transaction)
    column = query.available_columns.find {|col| col.name == column_name.to_sym }
    unless column.nil?
      column.value(transaction)
    else
      ""
    end
  end

  def sum_of_transaction_values(transactions, custom_field_id)
    transactions.inject(0.00) {|sum, transaction|
      cv = transaction.custom_value_for(custom_field_id)
      return if cv.nil?
      sum + cv.value.to_f
    }
  end

  def formatted_number_to_currency(number)
    number = number.to_f
    if number > 0
      cls = "positive"
    elsif number < 0
      cls = "negative"
    else
      cls = ""
    end

    content_tag "span", number_to_currency(number), class: cls
  end

  def short_date_for_week(week, year)
    if week > 0
      date = Date.strptime("#{week}-#{year}", "%W-%Y")
    else
      date = Date.new(year) - 1
    end

    I18n.l date, format: :short
  end

  # Return custom field html tag corresponding to its format
  def custom_field_tag_with_options(prefix, custom_value, options = {})
    custom_value.custom_field.format.edit_tag self,
      custom_field_tag_id(prefix, custom_value.custom_field),
      custom_field_tag_name(prefix, custom_value.custom_field),
      custom_value,
      {:class => "#{custom_value.custom_field.field_format}_cf"}.merge(options)
  end

  def cashflow_select_tag(description, params, query)
    available_filters = query.available_filters[description]

    unless available_filters.nil?
      select_tag description, options_for_select([[""]] + available_filters[:values],  params[description])
    end
  end

end
