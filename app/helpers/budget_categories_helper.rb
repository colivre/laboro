module BudgetCategoriesHelper
  def default_budget_categories_set
    data = [
      :recurring_expenses,
      [
        :dairies,
        :earnings_and_fixed_allowances,
        :employer_taxes,
        :scholarships,
        [
          :financial_support_for_student,
          :financial_support_for_researcher,
        ],
        :interns,
        :services_from_individuals,
        :services_from_companies,
        :consumption_material,
        :travel_tickets,
      ],
      :capital_expenses,
      [
        :construction_and_installations,
        :equipment_and_permanent_material,
      ],
    ]
    format_budget_categories(data)
  end

  def format_budget_categories(data, level = 0)
    data.map do |item|
      if item.is_a?(Array)
        format_budget_categories(item, level + 1)
      else
        label = [:budget_categories_default_set, item].join('.')
        '  ' * level + I18n.t(label)
      end
    end.join("\n")
  end

end
