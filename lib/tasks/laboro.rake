namespace :laboro do

  desc 'Runs laboro tests'
  task :test do
    sh 'testrb', *Dir.glob('plugins/laboro/test/**/*.rb')
  end

end
