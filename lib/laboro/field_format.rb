module Redmine
  module FieldFormat

    class BudgetItemFormat < RecordList
      add 'budget_item'
      self.form_partial = 'custom_fields/formats/budget_item'

      def label
        "budget_item"
      end

      def possible_values_options(custom_field, object=nil)
        if object.is_a?(Array)
          projects = object.map {|o| o.respond_to?(:project) ? o.project : nil}.compact.uniq
          projects.map {|project| possible_values_options(custom_field, project)}.reduce(:&) || []
        elsif object.respond_to?(:project) && object.project
          categories = BudgetCategory.by_project(object.project)
          items = categories.map(&:items).flatten
          items.sort_by(&:description).collect {|item| [item.description, item.id.to_s]}
        else
          []
        end
      end

    end


  end
end
