module Laboro
  module IssueFixBudgetValues
    module Patches
      module IssuePatch
        include CashflowHelper

        def self.included(base)
          base.class_eval do
            unloadable

            before_save :fix_budget_values

            protected
            def custom_field_value_for(id)
              custom_field_values.detect {|v| v.custom_field_id == id }
            end

            def fix_budget_values
              # Return if Cashflow is not configured for the DB
              return if Cashflow.transaction_type_custom_field.nil? || Cashflow.transaction_custom_field.nil?

              type_field = self.custom_field_value_for(Cashflow.transaction_type_custom_field.id)
              value_field = self.custom_field_value_for(Cashflow.transaction_custom_field.id)

              # Return unless it is a transaction
              return if type_field.nil? || value_field.nil?

              type = type_field.value
              value = value_field.value.to_i

              if(((["Income","Receita"].include?(type)) && (value < 0)) || ((["Expense", "Despesa"].include?(type)) && (value > 0)))
                 value_field.value = (-value).to_s
              end
            end
          end
        end
      end
    end
  end
end
