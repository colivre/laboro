  module Laboro
    module PDF
      DEFAULT_HEIGHT = 10
      BOTTOM_BORDER = 30
      include Redmine::Export::PDF

      class ExceededPageHeight < Exception

      end

      class LABOROCPDF < TCPDF
        include ::ActionView::Helpers::NumberHelper

        attr_accessor :header, :page_height, :col_width, :table_width

        def DrawBottomBorder
          Cell(col_width * (header.size + 1), 0, '', 'T')
        end

        def pdf_draw_borders(top_x, top_y, lower_y)
          col_x = top_x

          header.each_with_index do |head, i|
            col_x += i == 0 ? col_width * 2 : col_width
            Line(col_x, top_y, col_x, lower_y)  # columns right border
          end
          Line(top_x, top_y, top_x, lower_y)    # left border
          Line(top_x, lower_y, col_x, lower_y)  # bottom border
        end

        def LaboroCell(row, width, fill, nextmove="right", align="R", height=DEFAULT_HEIGHT)
          nextmove = nextmove == "right" ? 0 : 1;
          MultiCell(width, height, row.to_s, 0, align, fill,  nextmove)
        end

        def DrawLines(column_header, columns, fill)
          base_y = 0
          base_x = GetX()
          begin
            maxh = LaboroCell(column_header, col_width * 2, fill, "right", "L")

            columns.each() { |transaction|
              base_y = GetY()
              h = LaboroCell(number_to_currency(transaction), col_width, fill)
              maxh = [h, maxh].max

              raise ExceededPageHeight if base_y + maxh + BOTTOM_BORDER > page_height
            }
            pdf_draw_borders(base_x, base_y, base_y + maxh)
            SetY(base_y + maxh)

          rescue ExceededPageHeight
            SetFillColor(255, 255, 255)
            Rect(base_x, base_y, table_width, maxh, 'F')

            SetX(base_x)
            DrawBottomBorder()

            AddPage("L")
            WriteHeader()

            SetFillColor(224, 235, 255)
            SetTextColor(0)
            SetFont('')
            maxh = 0
            retry
          end
        end

        def WriteHeader
          SetFillColor(0, 0, 0)
          SetTextColor(255, 255, 255)
          SetDrawColor(0, 0, 0)
          SetLineWidth(0.2)
          SetFont('', 'B')

          if header.size > 0
            header.each_with_index {  |row, i|
              Cell(i == 0 ? col_width * 2 : col_width, 7, row, 1, 0, 'C', 1)
            }
            Ln()
          end

        end

        def ColoredTable(data, fill)
          SetFillColor(224, 235, 255)
          SetTextColor(0)
          SetFont('')

          DrawLines(I18n.t(data[0]), data[1], fill)

          unless data[2].nil?
            data[2].each {  |subject, transactions|
              DrawLines(subject, transactions, fill)
            }
          end
        end


        def MultiCell(w, h, txt, border=0, align='J', fill=0, ln=1)
          # save current position
          prevx = @x;
          prevy = @y;
          prevpage = @page;

          #Output text with automatic or explicit line breaks

          if (w == 0)
            w = @w - @r_margin - @x;
          end

          wmax = (w - 3 * @c_margin);

          s = txt.gsub("\r", ''); # remove carriage returns
          nb = s.length;

          b=0;
          if (border)
            if (border==1)
              border='LTRB';
              b='LRT';
              b2='LR';
            elsif border.is_a?(String)
              b2='';
              if (border.include?('L'))
                b2 << 'L';
              end
              if (border.include?('R'))
                b2 << 'R';
              end
              b=(border.include?('T')) ? b2 + 'T' : b2;
            end
          end
          sep=-1;
          to_index=0;
          from_j=0;
          l=0;
          ns=0;
          nl=1;

          while to_index < nb
            #Get next character
            c = s[to_index];
            if c == "\n"[0]
              #Explicit line break
              if @ws > 0
                @ws = 0
                out('0 Tw')
              end
              #Ed Moss - change begin
              end_i = to_index == 0 ? 0 : to_index - 1
              # Changed from s[from_j..to_index] to fix bug reported by Hans Allis.
              from_j = to_index == 0 ? 1 : from_j
              Cell(w, h, s[from_j..end_i], b, 2, align, fill)
              #change end
              to_index += 1
              sep=-1
              from_j=to_index
              l=0
              ns=0
              nl += 1
              b = b2 if border and nl==2
              next
            end
            if (c == " "[0])
              sep = to_index;
              ls = l;
              ns += 1;
            end

            l = GetStringWidth(s[from_j, to_index - from_j]);

            if (l > wmax)
              #Automatic line break
              if (sep == -1)
                if (to_index == from_j)
                  to_index += 1;
                end
                if (@ws > 0)
                  @ws = 0;
                  out('0 Tw');
                end
                Cell(w, h, s[from_j..to_index-1], b, 2, align, fill) # my FPDF version
              else
                if (align=='J' || align=='justify' || align=='justified')
                  @ws = (ns>1) ? (wmax-ls)/(ns-1) : 0;
                  out(sprintf('%.3f Tw', @ws * @k));
                end
                Cell(w, h, s[from_j..sep], b, 2, align, fill);
                to_index = sep + 1;
              end
              sep=-1;
              from_j = to_index;
              l=0;
              ns=0;
              nl += 1;
              if (border and (nl==2))
                b = b2;
              end
            else
              to_index += 1;
            end
          end
          #Last chunk
          if (@ws>0)
            @ws=0;
            out('0 Tw');
          end
          if (border.is_a?(String) and border.include?('B'))
            b << 'B';
          end
          Cell(w, h, s[from_j, to_index-from_j], b, 2, align, fill);

          # move cursor to specified position
          # since 2007-03-03
          if (ln == 1)
            # go to the beginning of the next line
            @x = @l_margin;
          elsif (ln == 0)
            # go to the top-right of the cell
            @page = prevpage;
            diff_y = @y - prevy
            @y = prevy;
            @x = prevx + w;
            return diff_y
          elsif (ln == 2)
            # go to the bottom-left of the cell
            @x = prevx;
          end
          return 0
        end
      end

      def report_to_pdf(credits, debts, period_balance, current_balance)
        pdf = LABOROCPDF.new()
        title = I18n.t(:cashflow_report)

        pdf.SetTitle(title)
        pdf.alias_nb_pages
        pdf.SetAutoPageBreak(false)
        pdf.AddPage("L")

        # Landscape A4 = 210 x 297 mm
        pdf.page_height = 210
        page_width    = 297
        left_margin   = 10
        right_margin  = 10

        # column widths
        table_width = page_width - right_margin - left_margin
        col_width = table_width / 14

        # title
        pdf.SetFont("FreeSans", 'B', 9)
        pdf.LaboroCell(title, 190, 0, "down", "L")
        pdf.Ln

        header = I18n.t(:'date.month_names')

        pdf.header = header
        pdf.col_width = col_width
        pdf.table_width = table_width

        pdf.WriteHeader()
        pdf.ColoredTable(credits, 0);
        pdf.ColoredTable(debts, 0);
        pdf.ColoredTable(period_balance, 0);
        pdf.ColoredTable(current_balance, 0);

        pdf.DrawBottomBorder()


        pdf.Output
      end


    end
  end
