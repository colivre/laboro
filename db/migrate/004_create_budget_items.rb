class CreateBudgetItems < ActiveRecord::Migration
  def change
    create_table :budget_items do |t|
      t.string :description
      t.decimal :quantity
      t.decimal :value
      t.integer :budget_category_id
    end
  end
end
