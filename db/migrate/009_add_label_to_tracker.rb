class AddLabelToTracker < ActiveRecord::Migration

  def up
    add_column :trackers, :label, :string

    Tracker.connection.schema_cache.clear!
    Tracker.reset_column_information

    transaction = Tracker.where(:name => "Fluxo de caixa").first

    transaction.name = I18n.t(:cashflow_title)
    transaction.label = "cashflow"

    transaction.save!

  end

  def down
    transaction = Tracker.where(:label => "cashflow").first

    remove_column :trackers, :label

    transaction.name = "Fluxo de caixa"

    transaction.save!
  end

end
