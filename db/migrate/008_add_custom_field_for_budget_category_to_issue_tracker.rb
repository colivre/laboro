class AddCustomFieldForBudgetCategoryToIssueTracker < ActiveRecord::Migration
  def up
    transaction = Tracker.where(:name => "Fluxo de caixa").first

    transaction_value = IssueCustomField.create!(:name => "Tipo da transacao", :field_format => "budget_item", :is_required => true, :is_for_all => true, :searchable => true, :is_filter => false, :position => 2, :default_value => "", :visible => true, :multiple => false, :format_store => {:edit_tag_style=>""})

    transaction.custom_fields << transaction_value
  end

  def down
    transaction_value = IssueCustomField.where(:name => "Tipo da transacao").first
    transaction_value.destroy unless transaction_value.nil?
  end

end
