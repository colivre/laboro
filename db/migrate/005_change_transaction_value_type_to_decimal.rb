class ChangeTransactionValueTypeToDecimal < ActiveRecord::Migration
  def up
    transaction = Tracker.where(:name => "Fluxo de caixa").first

    old_transaction_value = IssueCustomField.where(:name => "Valor de transacao").first
    old_transaction_value.destroy unless old_transaction_value.nil?

    transaction_value = IssueCustomField.create!(:name => "Valor de transacao", :field_format => "float", :is_required => true, :is_for_all => true, :is_filter => true, :searchable => true, :default_value => "0.00", :regexp => "^-?[0-9]+([,.][0-9]{2})?$")

    transaction.custom_fields << transaction_value
    transaction.reload

  end

  def down
    transaction = Tracker.where(:name => "Fluxo de caixa").first

    old_transaction_value = IssueCustomField.where(:name => "Valor de transacao").first
    old_transaction_value.destroy unless old_transaction_value.nil?

    transaction_value = IssueCustomField.create!(:name => "Valor de transacao", :field_format => "int", :is_required => true, :is_for_all => true, :is_filter => true, :searchable => true, :default_value => "0", :regexp => "")

    transaction.custom_fields << transaction_value
    transaction.reload
  end
end
