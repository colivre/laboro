class ChangeBudgetItemFilterableStatus < ActiveRecord::Migration
  def up
    IssueCustomField.where(label: 'budget_item').each do |field|
      field.is_filter = true

      field.save!
    end
  end

  # There is no need to change it back to any other value. We just want to
  # make sure it is filterable on up, it doesn't matter the down migration.
end
