class AddCustomFieldForTransactionTypeToIssueTracker < ActiveRecord::Migration
  def up
    transaction = Tracker.where(:label => "cashflow").first

    IssueCustomField.all.each_with_index {|custom_field, i| custom_field.update_attribute(:position, i + 2)}

    debt = I18n.t(:debts, count: 1)
    credit = I18n.t(:credits, count: 1)
    
    transaction_value = IssueCustomField.create!(
      :name => I18n.t('cashflow_transaction_type'),
      :label => "transaction_type", field_format: "list",
      possible_values: [debt, credit], regexp: "", min_length: nil,
      max_length: nil, is_required: true, is_for_all: true, is_filter: true,
      searchable: false, default_value: debt,
      editable: false, visible: true, multiple: false,
      format_store: {:url_pattern=>"", :edit_tag_style=>"check_box"})

    transaction_value.position = 1
    transaction_value.save!

    transaction.custom_fields << transaction_value
  end

  def down
    transaction_value = IssueCustomField.where(:label => "transaction_type").first
    transaction_value.destroy unless transaction_value.nil?

    IssueCustomField.all.each_with_index {|custom_field, i| custom_field.update_attribute(:position, i + 1)}
  end

end
