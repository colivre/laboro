class AddRemovedToBudgetItems < ActiveRecord::Migration
  def up
    add_column :budget_items, :removed, :boolean, default: false
    execute 'UPDATE budget_items SET removed = (1>2)'
  end

  def down
    remove_column :budget_items, :removed
  end

end
