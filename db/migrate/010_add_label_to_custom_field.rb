class AddLabelToCustomField < ActiveRecord::Migration
  def update_transaction_value_up(name, l10n_label, label)
    transaction_value = IssueCustomField.where(:name => name).first

    transaction_value.name = I18n.t(l10n_label)
    transaction_value.label = label

    transaction_value.save!
  end

  def up
    add_column :custom_fields, :label, :string

    IssueCustomField.connection.schema_cache.clear!
    IssueCustomField.reset_column_information

    update_transaction_value_up("Tipo da transacao", :cashflow_transaction_type, "transaction_type")
    update_transaction_value_up("Valor de transacao", :cashflow_transaction_value, "transaction_value")
  end

  def down
    transaction_type = IssueCustomField.where(:label => "transaction_type").first
    transaction_value = IssueCustomField.where(:label => "transaction_value").first

    remove_column :custom_fields, :label

    transaction_type.name = "Tipo da transacao"
    transaction_type.save!

    transaction_value.name = "Valor de transacao"
    transaction_value.save!
  end

end
