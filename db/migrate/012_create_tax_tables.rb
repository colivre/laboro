class CreateTaxTables < ActiveRecord::Migration
  def change
    create_table :tax_tables do |t|
      t.integer     :project_id
      t.string      :name
      t.boolean     :available, :default => true
      t.date        :valid_from
      t.date        :valid_until
      t.text        :entries
    end
  end
end
