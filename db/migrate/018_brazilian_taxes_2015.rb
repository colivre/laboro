# encoding: UTF-8

class BrazilianTaxes2015 < ActiveRecord::Migration
  def up
    pj_2015 = [
      { name: 'Alíquota ISS'           , variable: 'al_iss'    , expression: 'lambda { 0.02 }   # 2%'                                       },
      { name: 'Alíquota IR'            , variable: 'al_ir'     , expression: 'lambda { 0.015 }  # 1.5%'                                     },
      { name: 'Alíquota COFINS'        , variable: 'al_cofins' , expression: 'lambda { 0.03 }   # .65%'                                     },
      { name: 'Alíquota PIS'           , variable: 'al_pis'    , expression: 'lambda { 0.0065 } # 3%'                                       },
      { name: 'Alíquota INSS'          , variable: 'al_inss'   , expression: 'lambda { 0 }      # 0% (cooperativa de trabalho isenta)'      },
      { name: 'Piso PIS/COFINS'        , variable: 'piso_pc'   , expression: 'lambda { 5000 }'                                              },
      #################################################################################################
      { name: 'Bruto com COFINS e PIS' , variable: 'bruto_com' , expression: 'lambda { input / (1-al_iss-al_ir-al_cofins-al_pis-al_inss) }' },
      { name: 'Bruto sem COFINS e PIS' , variable: 'bruto_sem' , expression: 'lambda { input / (1-al_iss-al_ir-al_inss) }'                  },
      { name: 'Bruto'                  , variable: 'bruto'     , expression: 'lambda { (bruto_com > piso_pc) ? bruto_com : bruto_sem }'     },
      { name: 'ISS'                    , variable: 'iss'       , expression: 'bruto * al_iss'                                               },
      { name: 'IR'                     , variable: 'ir'        , expression: 'bruto * al_ir'                                                },
      { name: 'COFINS'                 , variable: 'cofins'    , expression: '(bruto > piso_pc) ? bruto * al_cofins : 0'                    },
      { name: 'PIS'                    , variable: 'pis'       , expression: '(bruto > piso_pc) ? bruto * al_pis : 0'                       },
      { name: 'INSS'                   , variable: 'inss'      , expression: 'bruto * al_inss'                                              },
    ]
    TaxTable.create!(
      name: 'Serviço PJ (cooperativa de trabalho, BA, 2015)',
      entries: pj_2015,
    )

    irpf_2015 = [
      { name: 'Alíquota ISS'            , variable: 'al_iss'    , expression: 'lambda { 0.05 } # 5%' },
      { name: 'Alíquota INSS patronal'  , variable: 'al_inss_p' , expression: 'lambda { 0.20 } # 20%' },
      { name: 'Tabela do INSS/IR'       , variable: 'tab_ir'    , expression: 'lambda { 2015 }' },
      #################################################################################################
      { name: 'Bruto'                   , variable: 'bruto'     , expression: 'lambda { set_irpf(tab_ir) && bruto_com_irpf_inss(input, al_iss) }' },
      { name: 'ISS'                     , variable: 'iss'       , expression: 'al_iss * bruto' },
      { name: 'INSS'                    , variable: 'inss'      , expression: 'inss(bruto)' },
      { name: 'IRPF'                    , variable: 'irpf'      , expression: 'irpf(bruto)' },
      { name: 'INSS Patronal'           , variable: 'inss_p'    , expression: 'al_inss_p * bruto' },
    ]
    TaxTable.create!(
      name: 'Serviço PF (INSS+IRPF+ISS, 2015)',
      entries: irpf_2015
    )
  end

  def down
    # not reversible for now
  end
end
