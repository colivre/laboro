class CreateBudgetSnapshots < ActiveRecord::Migration
  def change
    create_table :budget_snapshots do |t|
      t.integer :project_id
      t.text :data
      t.timestamps
    end
  end
end
