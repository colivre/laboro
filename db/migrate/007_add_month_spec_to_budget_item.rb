class AddMonthSpecToBudgetItem < ActiveRecord::Migration
  def up
    add_column :budget_items, :month_spec, :string
  end

  def down
    remove_column :budget_items, :month_spec
  end
end
