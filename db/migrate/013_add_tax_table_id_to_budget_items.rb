class AddTaxTableIdToBudgetItems < ActiveRecord::Migration
  def change
    add_column :budget_items, :tax_table_id, :integer
  end
end
