class RenameTransactionToCashflow < ActiveRecord::Migration
  def up
    transaction = Tracker.where(:name => "Transacao").first
    unless transaction.nil?
      transaction.name = "Fluxo de caixa"
      transaction.save!
    end
  end

  def down
    transaction = Tracker.where(:name => "Fluxo de caixa").first
    unless transaction.nil?
      transaction.name = "Transacao"
      transaction.save!
    end

  end
end
