class ChangeValidationToValueCustomField < ActiveRecord::Migration
  def change_regex(regex)
    transaction_value = IssueCustomField.where(:label => "transaction_value").first
    transaction_value.regexp = regex
    transaction_value.save!
  end

  def up
    # Now we are accepting either one or two digits after point/comma
    change_regex("^-?[0-9]+([,.][0-9]{1,2})?$")
  end

  def down
    change_regex("^-?[0-9]+([,.][0-9]{2})?$")
  end
end
