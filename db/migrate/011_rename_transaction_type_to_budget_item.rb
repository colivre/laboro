class RenameTransactionTypeToBudgetItem < ActiveRecord::Migration
  def up
    IssueCustomField.where(label: 'transaction_type').each do |field|
      field.name = I18n.t('cashflow_budget_item')
      field.label = 'budget_item'
      field.save!
    end
  end

  def down
    IssueCustomField.where(label: 'budget_item').each do |field|
      field.name = I18n.t('cashflow_budget_item')
      field.label = 'transaction_type'
      field.save!
    end
  end

end
