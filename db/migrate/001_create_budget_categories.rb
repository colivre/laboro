class CreateBudgetCategories < ActiveRecord::Migration
  def change
    create_table :budget_categories do |t|
      t.string  :name
      t.integer :parent_id
      t.integer :project_id
    end
  end
end
