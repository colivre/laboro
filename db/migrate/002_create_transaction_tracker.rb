class CreateTransactionTracker < ActiveRecord::Migration
  def up
    # Create the transaction tracker
    transaction = Tracker.create!(:name => "Transacao", :is_in_chlog => false, :is_in_roadmap => false)

    # Initialize with some sane workflow
    transaction.workflow_rules.copy(Tracker.first)
    # TODO: check which status are relevant and remove the ones who aren't

    # Add to all projects
    transaction.projects << Project.all

    transaction.reload

    # Create temporary field for value of transaction
    # TODO: currently we accept negative values, soon we will switch to a type based approach
    transaction_value = IssueCustomField.create!(:name => "Valor de transacao", :field_format => "int", :is_required => true, :is_for_all => true, :is_filter => true, :searchable => true, :default_value => "0")

    transaction.custom_fields << transaction_value

    # Set the default fields for the transaction: category and milestone
    transaction.core_fields = %w(category_id fixed_version_id)
  end

  def down
    transaction = Tracker.where(:name => "Transacao").first
    transaction.destroy unless transaction.nil?

    transaction_value = IssueCustomField.where(:name => "Valor de transacao").first
    transaction_value.destroy unless transaction_value.nil?
  end
end
