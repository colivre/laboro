Redmine::Plugin.register :laboro do
  name 'Laboro plugin'
  author 'Colivre'
  description 'Project budget planning and cash flow for Redmine'
  version '0.12.3'
  url 'http://gitlab.com/colivre/laboro'
  author_url 'http://colivre.coop.br/'
  require_dependency 'laboro/field_format'

  project_module :budget do
    permission :budget_view, { :budget => [:index] }
  end

  project_module :cashflow do
    permission :cashflow_view, { :cashflow => [:index] }
  end

  menu :project_menu, :budget, { :controller => :budget, :action => :index }, :caption => :budget_title, :after => :activity, :param => :project_id
  menu :project_menu, :cashflow, { :controller => :cashflow, :action => :index }, :caption => :cashflow_title, :after => :budget, :param => :project_id

  ActionDispatch::Callbacks.to_prepare do
    require_dependency 'issue'
    Issue.send(:include, Laboro::IssueFixBudgetValues::Patches::IssuePatch)
  end

end
