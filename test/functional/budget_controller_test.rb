require File.expand_path('../../test_helper', __FILE__)

class BudgetControllerTest < ActionController::TestCase

  def test_index
    get :index, project_id: sample_project.identifier
  end

  def test_snapshot
    assert_difference 'BudgetSnapshot.count', +1 do
      post :snapshot, project_id: sample_project.identifier
    end
  end

  def test_compare
    project = sample_project

    snapshot1 = BudgetSnapshot.build(project).tap(&:save!)

    BudgetItem.last.update_attributes(value: 20)
    snapshot2 = BudgetSnapshot.build(project).tap(&:save!)

    get :compare_snapshots, project_id: project.identifier, from_id: snapshot1.id, to_id: snapshot2.id
  end

  def test_schedule
    project = sample_project
    get :schedule, project_id: project.identifier
  end

  def test_create_issues_preview
    get :create_issues_preview, project_id: sample_project.identifier, start_date: Date.today.to_s
  end

  def test_create_issues
    project = sample_project
    assert_difference 'Issue.count', 1 do
      post :create_issues, project_id: project.identifier, start_date: Date.today.to_s
    end
  end

end
