require File.expand_path('../../test_helper', __FILE__)

class TaxTablesControllerTest < ActionController::TestCase

  test 'new' do
    get :new, project_id: sample_project.identifier
  end

  test 'create' do
    assert_difference('TaxTable.count', 1) do
      post :new, project_id: sample_project.identifier, tax_table: { name: 'foo' }
    end
  end

  test 'edit' do
    project = sample_project
    table = TaxTable.create!(project: project, name: 'foo')
    get :edit, project_id: project.identifier, id: table.id
  end

  test 'update' do
    project = sample_project
    table = TaxTable.create!(project: project, name: 'foo')
    post :edit, project_id: project.identifier, id: table.id, tax_table: { name: 'bar'}
    table.reload
    assert_equal 'bar', table.name
  end

  test 'remove' do
    project = sample_project
    table = TaxTable.create!(project: project, name: 'foo')
    post :destroy, project_id: project.identifier, id: table.id
    assert_equal 0, TaxTable.where(project_id: project.id).count
  end

  test 'remove with budget items' do
    project = sample_project_with_budget_item
    table = TaxTable.create!(project: project, name: 'foo')
    item = BudgetCategory.by_project(project).first.items.first
    item.tax_table = table
    item.save!

    post :destroy, project_id: project.identifier, id: table.id
  end

end
