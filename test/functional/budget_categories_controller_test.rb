require File.expand_path('../../test_helper', __FILE__)

class BudgetCategoriesControllerTest < ActionController::TestCase
  test 'parse 1' do
    input = "one\n  oneone\ntwo\n"
    data = @controller.send(:parse, input)

    assert_equal 3, data.size
    assert_nil data[0].parent
    assert_equal data[0], data[1].parent
    assert_nil data[2].parent
  end

  test 'index' do
    get :index, project_id: sample_project.identifier
  end
end
