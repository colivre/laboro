require File.expand_path('../../test_helper', __FILE__)

class BudgetItemsControllerTest < ActionController::TestCase

  test 'redisplay edit screen after validation failed on new items' do
    project = sample_project

    category = budget_category_with_items
    category.name = "my category"
    category.project = project
    category.save!

    valid_data =  { quantity: 1, value: 1000, description: 'blablabla' }
    invalid_data = { description: 'foobar', quantity: 4, value: 1000, month_spec: 4 }

    category.items.create!(valid_data)
    valid_item = category.items.first

    items = [ valid_data.merge(id: valid_item.id), invalid_data ]
    post :edit, project_id: project.identifier, category_id: category.id, budget_items: items
  end

end
