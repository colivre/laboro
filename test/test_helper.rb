# Load the Redmine helper
require File.expand_path(File.dirname(__FILE__) + '/../../../test/test_helper')

class ActiveSupport::TestCase

  def budget_category_with_items(*values)
    category = BudgetCategory.new
    values.each do |v|
      category.items << BudgetItem.new(quantity: 1, value: v)
    end
    category
  end

  def budget_snapshot_with_subcategories
    categories = [budget_category_with_items(1)]
    categories.first.children << budget_category_with_items(2)
    snapshot = BudgetSnapshot.new
    snapshot.categories = categories
    snapshot
  end

  def sample_project
    project = Project.new(identifier: 'test-project', name: 'Test project')
    project.save!

    category = BudgetCategory.create!(project_id: project.id, name: 'foo')
    category.items << BudgetItem.new(description: 'foo', quantity: 1, value: BigDecimal.new('12.34'), month_spec: '1')

    # FIXME these were more or less copied from the migrations ...
    tracker = Tracker.create!(name: 'cash flow', label: 'cashflow')
    tracker.custom_fields << IssueCustomField.create!(:name => "Value", :label => 'transaction_value', :field_format => "float", :is_required => true, :is_for_all => true, :is_filter => true, :searchable => true, :default_value => "0")
    tracker.custom_fields << IssueCustomField.create!(:name => "Budget Item", :label => 'budget_item', :field_format => "budget_item", :is_required => true, :is_for_all => true, :searchable => true, :is_filter => false, :position => 2, :default_value => "", :visible => true, :multiple => false, :format_store => {:edit_tag_style=>""})
    tracker.custom_fields << IssueCustomField.create!(
      :name => I18n.t('cashflow_transaction_type'),
      :label => "transaction_type", field_format: "list",
      possible_values: ['Expense', 'Income'], regexp: "", min_length: nil,
      max_length: nil, is_required: true, is_for_all: true, is_filter: true,
      searchable: false, default_value: 'Expense',
      editable: false, visible: true, multiple: false,
      format_store: {:url_pattern=>"", :edit_tag_style=>"check_box"})

    IssueStatus.create!(name: 'New', is_closed: false, is_default: true)
    IssuePriority.create!(name: 'Normal', active: true, is_default: true)


    project
  end

  def sample_project_with_budget_item
    project = sample_project

    category = budget_category_with_items
    category.name = "my category"
    category.project = project
    category.save!

    category.items.create!(quantity: 1, value: 1000, description: 'blablabla')

    project
  end

end
