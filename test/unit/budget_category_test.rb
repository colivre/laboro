require File.expand_path('../../test_helper', __FILE__)

class BudgetCategoryTest < ActiveSupport::TestCase

  test 'total of the items' do
    assert_equal 5, budget_category_with_items(2,3).total
  end

  test 'total with unvalued items' do
    assert_equal 2, budget_category_with_items(2,nil).total
  end

  test 'total of subcategories' do
    c = BudgetCategory.new
    c.children << budget_category_with_items(2)
    c.children << budget_category_with_items(3)
    assert_equal 5, c.total
  end

end
