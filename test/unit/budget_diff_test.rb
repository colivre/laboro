require File.expand_path('../../test_helper', __FILE__)

class BudgetDiffTest < ActiveSupport::TestCase

  def setup
    @old_budget = build_budget
    @new_budget = build_budget
  end

  attr_reader :old_budget, :new_budget

  def diff(old=nil, new=nil)
    old ||= @old_budget
    new ||= @new_budget
    BudgetDiff.new(old, new)
  end

  def test_trivial
    assert diff.empty?
  end

  def test_removal
    item = BudgetItem.new(description: 'foo', quantity: 1, value: 10)
    old_budget.categories.first.items << item

    assert_equal [item], diff.removed
  end

  def test_addition
    item = BudgetItem.new(description: 'foo', quantity: 1, value: 10)
    new_budget.categories.first.items << item

    assert_equal [item], diff.added
  end

  def test_modification
    project = Project.create!(name: 'test-project', identifier: 'test-project')
    category = BudgetCategory.create!(project: project, name: 'foo')
    item1 = BudgetItem.create!(category: category, description: 'foo', quantity: 1, value: 10)
    item2 = BudgetItem.create!(category: category, description: 'bar', quantity: 1, value: 10)
    item3 = BudgetItem.create!(category: category, description: 'baz', quantity: 1, value: 10)

    snapshot = BudgetSnapshot.build(project)
    snapshot.save!

    item3.value = 20
    item3.save!

    project = Project.find(project.id)
    new_snapshot = BudgetSnapshot.build(project)
    new_snapshot.save!

    changed = diff(snapshot, new_snapshot).changed.first

    assert_equal [item3, item3], [changed.old, changed.new]
  end

  def test_identity_from_db
    saved = save_budget

    ref1 = BudgetSnapshot.find(saved.id)
    ref2 = BudgetSnapshot.find(saved.id)

    assert diff(ref1, ref2).empty?
  end

  private

  def build_budget
    BudgetSnapshot.new.tap do |budget|
      category = BudgetCategory.new(name: 'category 1')
      budget.categories = [category]
    end
  end

  def save_budget
    budget = build_budget
    item = BudgetItem.new(description: 'foo', quantity: 1, value: 10)
    budget.categories.first.project_id = 1
    budget.categories.first.items << item
    budget.categories.first.save!
    budget.project_id = 1
    budget.save!

    budget
  end

end

