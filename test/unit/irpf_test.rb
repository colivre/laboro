require File.expand_path('../../test_helper', __FILE__)

class IRPFTest < ActiveSupport::TestCase

  def setup
    @calc = Irpf.new(2015)
  end

  def test_inss
    assert_equal 80, @calc.inss(1000)
    assert_equal 180, @calc.inss(2000)
    assert_equal 440, @calc.inss(4000)
    assert_equal 513.01, @calc.inss(8000)
  end

  def test_irpf
    assert_equal 0, @calc.irpf(1000)
    assert_in_delta 7.20, @calc.irpf(2000), 0.01
    assert_in_delta 95.20, @calc.irpf(3000), 0.01
    assert_in_delta 263.87, @calc.irpf(4000), 0.01
    assert_in_delta 780.64, @calc.irpf(6000), 0.01
  end

  def test_irpf_inss_br
    assert_equal({ inss: 80, irpf: 0 }, @calc.irpf_inss_br(1000))
    assert_equal({ inss: 440.00, irpf: 179.20 }, @calc.irpf_inss_br(4000))
  end

  def test_liq
    assert_in_delta 7747.43, @calc.liq(10000), 0.01
  end

  def test_find_br
    assert_in_delta 13107.00, @calc.find_br(10000), 0.01
    assert_in_delta 6210.44, @calc.find_br(5000), 0.01
    assert_in_delta 2863.28, @calc.find_br(2500), 0.01
  end

end


