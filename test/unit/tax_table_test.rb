require File.expand_path('../../test_helper', __FILE__)

module TaxTestData
  INSS = {
    'name' => 'INSS',
    'variable' => 'inss',
    'expression' => 'input * 0.11',
  }
end

class TaxTableTest < ActiveSupport::TestCase

  include TaxTestData

  test 'entries is initialized as an array' do
    assert_instance_of Array, table.entries
  end

  test 'calculates tax' do
    table.entries = [INSS]
    assert_equal 110, table.taxes_for(1000).first.result
  end

  test 'multi-level taxes' do # e.g. Brazilian income taxes table
    table.entries = [INSS, { 'name' => 'IRPF', 'variable' => 'irpf', 'expression' => '0.2 * input - inss' }]
    assert_equal 90, table.taxes_for(1000).last.result
  end

  [
    # invalid expressions
    ['Project.destroy_all',         :assert_not_equal],
    ['system("rm -rf /")',          :assert_not_equal],
    ['raise "BOOM"',                :assert_not_equal],
    ['f(1)',                        :assert_not_equal],

    # valid expressions
    ['0.11 * input',                :assert_equal],
    ['input - 100',                 :assert_equal],
    ['lambda { |x| x + 1 }',        :assert_equal],
    # ->(x) { x + 1 } has exactly the same parse tree as above
    ['if (1==1) then 1 else 2 end', :assert_equal],
    ['(1==1) && 1 || 2',            :assert_equal],
    ['(1==1) ? 1 : 2',              :assert_equal],
    ['1; 2',                        :assert_equal],

    # Brazilian taxes
    ['set_irpf(2015)',              :assert_equal],
    ['bruto_com_irpf_inss(input)',  :assert_equal],
    ['inss(1000)',                  :assert_equal],
    ['irpf(1000)',                  :assert_equal],

    # TODO it might be useful to support hashes and arrays

  ].each do |expr, assertion|
    if assertion == :assert_not_equal
      test_name = 'does NOT accept ' + expr.inspect
    else
      test_name = 'accepts ' + expr.inspect
    end
    test test_name do
      table.entries = [
        {'name' => 'Test', 'variable' => 'y', 'expression' => expr}
      ]
      table.valid?
      send assertion, [], table.errors[:entries]
    end
  end

  test 'cannot reference variables that were not previously defined' do
    table.entries = [
      { 'name' => 'y', 'variable' => 'y', 'expression' => '0.3 * x'}
    ]
    table.valid?
    assert_not_equal [], table.errors[:entries]

    table.entries.unshift(
      # input can always be refenced
      { 'name' => 'x', 'variable' => 'x', 'expression' => 'input + 100'}
    )
    table.valid?
    assert_equal [], table.errors[:entries]
  end

  test 'can call previosly defined function with syntatic sugar' do
    table.entries = [
      { 'name' => 'f', 'variable' => 'f', 'expression' => '->(x) { x + 1 }' },
      { 'name' => 'y', 'variable' => 'y', 'expression' => 'f(input)'}
    ]
    taxes = table.taxes_for(1)
    # item with function must be discarded!
    assert_equal 2, taxes.first.result
  end

  private

  def table
    @table ||= TaxTable.new
  end

end

class TaxTableEntryTest < ActiveSupport::TestCase

  include TaxTestData

  test 'can be built from a hash' do
    entry = TaxTable::Entry.new(INSS)
    INSS.each do |k,v|
      assert_equal v, entry.send(k)
    end
  end

  test 'calculates expression on a given context' do
    entry = TaxTable::Entry.new(INSS)
    input = 100
    assert_equal 11, entry.evaluate!(binding)
  end

  test 'rounds up to 2 decimal places' do
    entry = TaxTable::Entry.new(
      'name' => 'A Third',
      'variable' => 'third',
      'expression' => 'input / 3.0'
    )
    input = 1.0
    assert_equal 0.33, entry.evaluate!(binding)

    input = 2.0
    assert_equal 0.67, entry.evaluate!(binding)
  end

  # TODO: prevent infinite loops
  # TODO: catch exceptions (e.g. divide by zero)

end
