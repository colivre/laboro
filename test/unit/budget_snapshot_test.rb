require File.expand_path('../../test_helper', __FILE__)

class BudgetSnapshotTest < ActiveSupport::TestCase

  def test_build_empty_project
    # is this does not crash we are good
    BudgetSnapshot.build(Project.new)
  end

  def test_total_empty_project
    assert_equal 0, BudgetSnapshot.build(Project.new).total
  end

  def test_total
    assert_equal 3, BudgetSnapshot.build(BudgetSnapshot::ProjectWrapper.new(nil, [budget_category_with_items(1,2)])).total
  end

  def test_load_categories
    snapshot = BudgetSnapshot.new
    snapshot.data = [{ 'name' => 'my category' }]
    assert_equal 'my category', snapshot.categories.first.name
  end

  def test_save_categories
    snapshot = BudgetSnapshot.new
    snapshot.categories = [BudgetCategory.new(name: 'my category')]
    assert_equal 'my category', snapshot.data.first['name']
  end

  def test_save_items
    snapshot = BudgetSnapshot.new
    snapshot.categories = [budget_category_with_items(1)]
    assert_equal 1.0, snapshot.data.first['items'].first['value']
  end

  def test_serialize_items_on_subcategories
    snapshot = budget_snapshot_with_subcategories
    assert_kind_of Enumerable, snapshot.data.first['children'].first['items']
  end

  def test_serialization_roundtrip
    project = Project.create!(name: 'test-project', identifier: 'test-project')
    category = BudgetCategory.create!(project: project, name: 'foo')
    item = BudgetItem.create!(category: category, description: 'foo', quantity: 1, value: 10)

    snapshot = BudgetSnapshot.build(project)
    snapshot.save!

    snapshot_from_db = BudgetSnapshot.find(snapshot.id)
    category = snapshot_from_db.categories.first
    assert_equal 1, category.items.size
    assert_not_nil category.id

    item = category.items.first
    assert_not_nil item.id
  end

  def test_previous_new
    assert_nil BudgetSnapshot.new.previous
  end

  def test_previous
    previous = BudgetSnapshot.create!(project_id: 99)
    assert_equal previous, BudgetSnapshot.new(project_id: 99).previous
  end

  def test_previous_is_not_self
    snapshot = BudgetSnapshot.create!(project_id: 99)
    assert_not_equal snapshot, snapshot.previous
  end

  def test_previous_new
    assert_nil BudgetSnapshot.new.previous
  end

  def test_previous
    previous = BudgetSnapshot.create!(project_id: 99)
    assert_equal previous, BudgetSnapshot.new(project_id: 99).previous
  end

  def test_previous_is_not_self
    snapshot = BudgetSnapshot.create!(project_id: 99)
    assert_not_equal snapshot, snapshot.previous
  end

  def test_previous_is_older
    first = BudgetSnapshot.create!(project_id: 99)
    second = BudgetSnapshot.create!(project_id: 99)
    assert_not_equal second, first.previous
  end

end
