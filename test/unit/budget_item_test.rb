require File.expand_path('../../test_helper', __FILE__)

class BudgetItemTest < ActiveSupport::TestCase

  test 'create in batch' do
    before = BudgetItem.count
    BudgetItem.batch(category, [
      { description: 'First item', quantity: 1, value: 10.0 },
      { description: 'Second item', quantity: 2, value: 20.0 },
    ]
    )
    assert_equal before + 2, BudgetItem.count
    assert_equal category, BudgetItem.last.category
  end

  test 'update in batch' do
    item = create_item!(quantity: 5)
    BudgetItem.batch(category, [item.attributes.merge('quantity' => 10)])

    item.reload
    assert_equal 10, item.quantity
  end

  test 'create and update in batch' do
    i1 = { description: 'foo', quantity: 1, value: 2 }
    i2 = { description: 'bar', quantity: 2, value: 4 }

    before = BudgetItem.count

    BudgetItem.batch(category, [i1])
    i1.merge!('id' => BudgetItem.last.id)

    BudgetItem.batch(category, [i1,i2])

    assert_equal before + 2, BudgetItem.count
  end

  test 'empty rows in batch' do
    before = BudgetItem.count
    BudgetItem.batch(category, [{}])
    assert_equal before, BudgetItem.count
  end

  test 'row with empty description in batch' do
    before = BudgetItem.count
    BudgetItem.batch(category, [{:description => ''}])
    assert_equal before, BudgetItem.count
  end

  test 'delete' do
    before = category.items.count
    item = create_item!
    BudgetItem.batch(category, [{ 'id' => item.id, '_delete' => 1}])

    assert_equal before, category.items.count
  end

  test 'total' do
    item = BudgetItem.new(quantity: 2, value: 3)
    assert_equal 6, item.total
  end

  test 'total error handling' do
    assert_equal nil, BudgetItem.new.total
    assert_equal nil, BudgetItem.new(quantity: 1).total
    assert_equal nil, BudgetItem.new(value: 1).total
  end

  {
    '1,2,3'   => [1, 2, 3],
    '1-3'     => [1, 2, 3],
    '1-6,12'  => [1, 2, 3, 4, 5, 6, 12],
    '1-6 12'  => [1, 2, 3, 4, 5, 6, 12],
    '1 6'     => [1, 6],
    '1-'      => [1],
    '-1'      => [1],
    'invalid' => [],
    nil       => [],
  }.each do |input, output|
    test "months #{input} = #{output}" do
      item = BudgetItem.new(month_spec: input)
      assert_equal output, item.months
    end
  end

  test 'quantity X months' do
    item = BudgetItem.new(quantity: 1, month_spec: '1-2')
    item.valid?
    assert_not_equal [], item.errors[I18n.t('budget_items.quantity')]
  end

  test 'taxes' do
    item = BudgetItem.new(quantity: 1, value: 100)
    item.tax_table = TaxTable.new(entries: [{ name: 'foo', variable: 'foo', expression: 'input * 0.1'}])

    assert_equal 110, item.total
  end

  test 'create journal on create' do
    project = sample_project
    category = BudgetCategory.new(project: project, name: 'test')
    item = BudgetItem.new(quantity: 1, value: 100, description: 'test', category: category)
    item.save!
    assert_equal 1, item.journals.count
  end

  test 'create journal on update' do
    project = sample_project
    category = BudgetCategory.new(project: project, name: 'test')
    item = BudgetItem.new(quantity: 1, value: 100, description: 'test', category: category)
    item.save!

    item.value = 120
    item.save!
    assert_equal 2, item.journals.count

    detail = item.journals.last.details.first
    assert_equal 'value', detail.prop_key
    assert_equal 100, detail.old_value.to_i
    assert_equal 120, detail.value.to_i
  end

  test 'log as removed when removed' do
    project = sample_project
    category = BudgetCategory.new(project: project, name: 'test')
    item = BudgetItem.new(quantity: 1, value: 100, description: 'test', category: category)
    item.save!

    item.removed = true
    item.save!

    journal = item.journals.last
    assert_equal 'removed', journal.notes
    assert journal.details.empty?
  end

  private

  def category
    @project ||= Project.create!(name: 'The project', identifier: 'the-project')
    @category ||= BudgetCategory.create!(name: 'The category', project: @project)
  end

  def item_data(attr = {})
    { budget_category_id: category.id, description: 'test item', quantity: 1, value: 10.0 }.merge(attr)
  end

  def create_item!(attr = {})
    BudgetItem.create!(item_data(attr))
  end

end
