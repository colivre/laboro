require File.expand_path('../../test_helper', __FILE__)

class CreateIssuesFromBudgetTest < ActiveSupport::TestCase

  test 'build from a budget' do
    budget = BudgetSnapshot.build(sample_project)
    CreateIssuesFromBudget.new(budget, Date.today)
  end

  test 'actually creating the issues' do
    budget = BudgetSnapshot.build(sample_project)
    creator = CreateIssuesFromBudget.new(budget, Date.today)
    assert_difference 'Issue.count', 1 do
      creator.create_issues!
    end
  end

  test 'items with taxes' do
    project = sample_project
    category = BudgetCategory.last
    item = category.items.last
    item.tax_table = TaxTable.create!(name: 'foo', entries: [{:name => 'foo', :variable => 'foo', :expression => 'input * 0.1'}])
    item.save!

    budget = BudgetSnapshot.build(project)
    creator = CreateIssuesFromBudget.new(budget, Date.today)

    assert_difference 'Issue.count', 2 do
      creator.create_issues!
    end
  end

end
