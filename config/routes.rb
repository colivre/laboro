# Plugin's routes
# See: http://guides.rubyonrails.org/routing.html
resources :projects do
  get 'budget', to: 'budget#index'
  post 'budget', to: 'budget#snapshot'

  get 'budget/snapshots', to: 'budget#history'
  get 'budget/snapshots/:id', to: 'budget#view_snapshot'
  get 'budget/snapshots/:from_id..:to_id', to: 'budget#compare_snapshots'
  get 'budget/snapshots/:from_id..', to: 'budget#compare_snapshots'
  get 'budget/schedule', to: 'budget#schedule'
  get 'budget/create_issues', to: 'budget#create_issues_preview'
  post 'budget/create_issues', to: 'budget#create_issues'

  get 'budget/categories', to: 'budget_categories#index'
  match 'budget/categories/bulk_add', to: 'budget_categories#bulk_add'
  match 'budget/categories/:action', controller: 'budget_categories'
  match 'budget/categories/:action/:id', controller: 'budget_categories'

  match 'budget/items/edit/:category_id', controller: 'budget_items', action: 'edit'
  match 'budget/items/:id', controller: 'budget_items', action: 'show'

  get 'cashflow', to: 'cashflow#report'
  get 'cashflow/list', to: 'cashflow#index'
  match 'cashflow/issues/:action', controller: 'cashflow_issues'
  match 'cashflow/issues/:action/:id', controller: 'cashflow_issues'

  match 'taxes', controller: 'tax_tables', action: 'index'
  match 'taxes/:action', controller: 'tax_tables'
  match 'taxes/:action/:id', controller: 'tax_tables'
  match 'cashflow_issues/update_form', :controller => 'cashflow_issues', :action => 'update_form', :via => [:put, :post], :as => 'cashflow_issue_form'
end
